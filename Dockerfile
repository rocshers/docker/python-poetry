ARG PYTHON_TAG=3.7-slim
ARG POETRY_VERSION=1.2.2

FROM python:3.7-slim as pre_base

RUN apt update -y && \
    apt install -y curl && \
    rm -rf /var/lib/{cache,log}/ && \
    rm -rf /var/lib/apt/lists/* && \
    rm -r /var/log

RUN curl -o install-poetry.py https://install.python-poetry.org

FROM python:$PYTHON_TAG as base

COPY --from=pre_base install-poetry.py install-poetry.py

# https://github.com/python-poetry/poetry/discussions/1879#discussioncomment-216865
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    # https://python-poetry.org/docs/configuration/#using-environment-variables
    POETRY_HOME=/opt/poetry \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    PATH=/opt/poetry/bin:$PATH

RUN python install-poetry.py && \
    poetry --version && \
    rm -r install-poetry.py
