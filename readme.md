# python-poetry

Python image with preset Poetry.
The image tag is composed of `{Version of base python image}-{Version of poetry}`

[![PyPI](https://img.shields.io/docker/pulls/rocshers/python-poetry)](https://hub.docker.com/r/rocshers/python-poetry)
[![GitLab stars](https://img.shields.io/gitlab/stars/rocshers/docker/python-poetry)](https://gitlab.com/rocshers/docker/python-poetry)
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/rocshers/docker/python-poetry)](https://gitlab.com/rocshers/docker/python-poetry)

## Usage

You can use all available poetry commands at once

```Dockerfile
FROM rocshers/python-poetry:3.12.4-slim-1.8.0

COPY pyproject.toml poetry.lock ./

RUN poetry install && \
    poetry cache clear --all -n .

COPY . .
```

## Links

Source Code: <https://gitlab.com/rocshers/docker/python-poetry>  
Issue Tracker: <https://gitlab.com/rocshers/docker/python-poetry/-/issues>  
Dockerfile: <https://gitlab.com/rocshers/docker/python-poetry/-/blob/release/Dockerfile>  
Docker hub: <https://hub.docker.com/r/rocshers/python-poetry>  
